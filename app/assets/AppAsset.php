<?php
namespace app\assets;

class AppAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@app/media';
    public $css = [
        'css/styles.css',
        'css/vendorBundle.css',
        'css/custom.css',
    ];
    public $js = [
        'js/scripts.js',
       // 'js/vendorBundle.js'
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
        'app\depends\owl_carousel\OwlAsset',
    ];
}
