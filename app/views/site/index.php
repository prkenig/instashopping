<?php
use yii\helpers\Url;

$this->title = 'instashopping';
?>


<div class="b-page__content__inner b-page__content__inner_content" data-reactid=".18zh28i2mwu.0.3">
    <div class="b-page__content__inner b-page__content__inner_content"
         data-reactid=".18zh28i2mwu.0.3.0">
        <div class="b-article" data-reactid=".18zh28i2mwu.0.3.0.0">
            <div class="b-text" data-reactid=".18zh28i2mwu.0.3.0.0.0"><h1
                        data-reactid=".18zh28i2mwu.0.3.0.0.0.0">instashopping</h1></div>
        </div>
        <section class="b-item-list" data-reactid=".18zh28i2mwu.0.3.0.1"><span
                    data-reactid=".18zh28i2mwu.0.3.0.1.0"><div class="b-slider b-slider_promo"
                                                               data-reactid=".18zh28i2mwu.0.3.0.1.0.0"><div
                            class="b-slider-slides" data-reactid=".18zh28i2mwu.0.3.0.1.0.0.0"
                            style="opacity: 0;"></div></div></span></section>
        <article class="b-article" data-reactid=".18zh28i2mwu.0.3.0.2">
            <div class="b-text" data-reactid=".18zh28i2mwu.0.3.0.2.0">
                <div id="frisbuy-widget">&nbsp;

                   <?=$this->render('_fotoshmoto')?>

                </div>
            </div>
        </article>
    </div>
</div>
<div class="b-page__content__inner b-page__content__inner_footer" data-reactid=".18zh28i2mwu.0.4">
    <div class="b-item-list" data-reactid=".18zh28i2mwu.0.4.0">
        <noscript data-reactid=".18zh28i2mwu.0.4.0.0"></noscript>
    </div>
    <div data-reactid=".18zh28i2mwu.0.4.1"></div>
    <footer class="b-footer" data-reactid=".18zh28i2mwu.0.4.2">
        <div class="b-footer__container" data-reactid=".18zh28i2mwu.0.4.2.0">
            <div class="b-footer__content" data-reactid=".18zh28i2mwu.0.4.2.0.0">
                <div class="b-footer__content-wrapper b-footer__content-wrapper--with-middle"
                     data-reactid=".18zh28i2mwu.0.4.2.0.0.0">
                    <div class="b-footer__nav b-footer__nav_main"
                         data-reactid=".18zh28i2mwu.0.4.2.0.0.0.0"><a class="b-footer__link"
                                                                      href="/dostavka"
                                                                      id="menu_item_link_55-content_page"
                                                                      data-reactid=".18zh28i2mwu.0.4.2.0.0.0.0.$menu-bottom-link-55-content_page">Доставка</a><a
                                class="b-footer__link" href="/blog"
                                id="menu_item_link_957-menu_item_blog"
                                data-reactid=".18zh28i2mwu.0.4.2.0.0.0.0.$menu-bottom-link-957-menu_item_blog">Блог</a><a
                                class="b-footer__link" href="/pages/5390-informatsiya-o-kompanii"
                                id="menu_item_link_5390-content_page"
                                data-reactid=".18zh28i2mwu.0.4.2.0.0.0.0.$menu-bottom-link-5390-content_page">Информация
                            о компании</a><a class="b-footer__link"
                                             href="/pages/5391-pravila-oplaty-garantii-bezopasnosti"
                                             id="menu_item_link_5391-content_page"
                                             data-reactid=".18zh28i2mwu.0.4.2.0.0.0.0.$menu-bottom-link-5391-content_page">Правила
                            оплаты | Гарантии безопасности</a><a class="b-footer__link"
                                                                 href="/pages/5392-politika-konfidentsialnosti-personalnyh-dannyh"
                                                                 id="menu_item_link_5392-content_page"
                                                                 data-reactid=".18zh28i2mwu.0.4.2.0.0.0.0.$menu-bottom-link-5392-content_page">Политика
                            конфиденциальности персональных данных</a></div>
                    <div class="b-footer__nav b-footer__nav_middle"
                         data-reactid=".18zh28i2mwu.0.4.2.0.0.0.1">
                        <div class="logi"></div>
                    </div>
                    <div class="b-footer__nav b-footer__nav_soc"
                         data-reactid=".18zh28i2mwu.0.4.2.0.0.0.3"><a class="b-footer__link"
                                                                      href="https://www.facebook.com/varvaradesign"
                                                                      id="menu_item_link_152-menu_item_link"
                                                                      data-reactid=".18zh28i2mwu.0.4.2.0.0.0.3.0:$menu-bottom-link-152-menu_item_link">facebook</a><a
                                class="b-footer__link" href="https://instagram.com/varvara_design/"
                                id="menu_item_link_155-menu_item_link"
                                data-reactid=".18zh28i2mwu.0.4.2.0.0.0.3.0:$menu-bottom-link-155-menu_item_link">instagram</a>
                        <noindex data-reactid=".18zh28i2mwu.0.4.2.0.0.0.3.1"><a
                                    href="https://kiiiosk.ru/?utm_source=kiosk_shop&amp;utm_campaign=varvara-shop.com"
                                    id="made_in_kiiiosk" rel="nofollow" target="_blank"
                                    data-reactid=".18zh28i2mwu.0.4.2.0.0.0.3.1.0">Сделано на
                                «Киоске»</a></noindex>
                        <noscript data-reactid=".18zh28i2mwu.0.4.2.0.0.0.3.2"></noscript>
                        <select class="LocaleSwitcher" data-reactid=".18zh28i2mwu.0.4.2.0.0.0.3.3">
                            <option selected="" data-url="/instashopping" value="ru"
                                    data-reactid=".18zh28i2mwu.0.4.2.0.0.0.3.3.$ru">Русский
                            </option>
                            <option data-url="/en/instashopping" value="en"
                                    data-reactid=".18zh28i2mwu.0.4.2.0.0.0.3.3.$en">English
                            </option>
                        </select></div>
                </div>
            </div>
        </div>
    </footer>
</div>
<div data-reactid=".18zh28i2mwu.0.5">
    <div class="Userbar" data-reactid=".18zh28i2mwu.0.5.0"></div>
    <noscript data-reactid=".18zh28i2mwu.0.5.1"></noscript>
    <noscript data-reactid=".18zh28i2mwu.0.5.2"></noscript>
</div>