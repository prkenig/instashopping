<?php
$get = Yii::$app->request->get();

$width = empty($get['width']) ? '20%' : $get['width'];
$margin = empty($get['margin']) ? '4px' : $get['margin'];
$margin_bottom = empty($get['margin_bottom']) ? '4px' : $get['margin_bottom'];
$font_size = empty($get['font_size']) ? '14px' : $get['font_size'];

$images = [
    'https://scontent-frx5-1.cdninstagram.com/t51.2885-15/sh0.08/e35/p640x640/21224131_1553947887960017_2952427933077078016_n.jpg',
    'https://scontent-frx5-1.cdninstagram.com/t51.2885-15/sh0.08/e35/p640x640/21148163_345819599207202_8086783721643966464_n.jpg',
    'https://scontent-frx5-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/21224698_1926675794239106_2250514759470284800_n.jpg',
    'https://scontent-frx5-1.cdninstagram.com/t51.2885-15/sh0.08/e35/p640x640/21107616_1593680027382602_982621601734328320_n.jpg',
    'https://scontent-frx5-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/21107384_1968727763411384_6772110166667034624_n.jpg',
    'https://scontent-frt3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/20987214_528445440838125_8762004768722780160_n.jpg',
    'https://scontent-frt3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c0.135.1080.1080/20902791_1973514039604388_4889384606488330240_n.jpg',
    'https://scontent-frt3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c0.135.1080.1080/20838886_971251446347385_4154305592342609920_n.jpg',
    'https://scontent-frt3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c181.0.718.718/20902569_1396553250423012_8549254087701430272_n.jpg',
    'https://scontent-frt3-2.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/20836938_1909175559350366_3083901718140813312_n.jpg',
    'https://scontent-frt3-2.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c0.135.1080.1080/20837774_341065252999904_2482065521663541248_n.jpg',
    'https://scontent-frt3-2.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c0.135.1080.1080/20838134_1843228532659408_288755878285279232_n.jpg',
    'https://scontent-frt3-2.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c0.135.1080.1080/20766671_1754893948144324_4552966385197645824_n.jpg',
    'https://scontent-frt3-2.cdninstagram.com/t51.2885-15/e35/20759204_667397586798950_7911141321836855296_n.jpg',
    'https://scontent-frt3-2.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/20688645_484971051862800_394133824310607872_n.jpg',
    'https://scontent-frt3-2.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/20686642_1175627315874970_8279186535539015680_n.jpg',
    'https://scontent-frt3-2.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/20687169_184206978787779_9189906897224859648_n.jpg',
    'https://scontent-frt3-2.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c1.0.1078.1078/20635095_875457942610723_8197917164364103680_n.jpg',
    'https://scontent-frt3-2.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c0.135.1080.1080/20688682_139359159995193_6223358992874209280_n.jpg',
    'https://scontent-frt3-2.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c0.135.1080.1080/20633974_108920813120646_7368626169974882304_n.jpg',
    'https://scontent-frt3-2.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/20635537_1875530452712845_3962200119033462784_n.jpg',
    'https://scontent-frt3-2.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c0.135.1080.1080/20590172_2000644396835024_7424292485184094208_n.jpg',
    'https://scontent-frt3-2.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/20481863_760693544132645_4290089467282194432_n.jpg',
    'https://scontent-frt3-2.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/20398595_1353377048115522_1522939714615640064_n.jpg',
    'https://scontent-frx5-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/20398893_825639740894499_5677443390849941504_n.jpg',
    'https://scontent-frx5-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c0.135.1080.1080/20479083_100845290618616_5529965346460532736_n.jpg',
    'https://scontent-frx5-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c0.134.1080.1080/20478993_763422533861497_3248693686233464832_n.jpg',
    'https://scontent-frx5-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c181.0.718.718/20393577_1915585702042219_5379794602389667840_n.jpg',
    'https://scontent-frx5-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c0.135.1080.1080/20398826_1143180805783135_8688954315684970496_n.jpg',
    'https://scontent.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c0.134.1080.1080/20394454_466016953775773_7027408958386077696_n.jpg',
    'https://scontent.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c0.135.1080.1080/20225428_265669977252544_4325451616657342464_n.jpg',
    'https://scontent.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c1.0.1078.1078/20347151_105522263461310_8246024169628631040_n.jpg',
    'https://scontent.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c0.135.1080.1080/20225406_139407026643414_613087666468028416_n.jpg',
    'https://scontent.cdninstagram.com/t51.2885-15/e15/c236.0.607.607/20347148_1763210447030359_8639714847803572224_n.jpg',
    'https://scontent.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c0.135.1080.1080/20180846_1929944263935772_7186653735697776640_n.jpg',
    'https://scontent.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c0.135.1080.1080/20067325_136820703571063_6894777293089013760_n.jpg',
    'https://scontent-lhr3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/20067451_116481788987670_795735273899032576_n.jpg',
    'https://scontent-lhr3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/20065939_760165587499256_5261911309326221312_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c0.135.1080.1080/20066013_117990718828117_5657949182833459200_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/19985480_152314221993633_1112530906305789952_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/20066192_310529432706984_8573215650116272128_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/19985893_1035956526534997_7258121695150473216_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c0.135.1080.1080/19932932_1375053029210540_2351925371189854208_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/19932497_1426678067399116_6797925419786960896_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c0.135.1080.1080/19931827_318587838584044_463365308392931328_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/19764749_1991493291082134_5420712240422584320_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c0.135.1080.1080/19764457_104416246876134_385306979322560512_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c0.134.1080.1080/19764343_324087044717731_812764217932251136_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/e35/19624582_110884026212938_2349824273188651008_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/19931684_1639883216032198_6628279773577609216_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c0.134.1080.1080/19623178_242305129616675_7956477214202003456_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c0.120.1080.1080/19761186_288543671617935_6991162866086707200_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c0.134.1080.1080/19623150_452038498516503_5514099466686169088_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c0.134.1080.1080/19623559_1926911294257916_6431618486723674112_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c104.0.871.871/19436545_116442825634274_8550378553089196032_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c0.134.1080.1080/19436252_1733486080277773_5198342151213154304_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/19533843_766677680170082_8673995743456919552_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c0.134.1080.1080/19437002_105145980116895_7930650149922013184_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/19534302_1565605076825732_2293918436243275776_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c0.134.1080.1080/19380025_1821587704822740_8102258201048645632_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c0.134.1080.1080/19379389_745442882331083_1611319021896990720_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c0.90.787.787/19429055_131281277454746_8576337941016608768_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c0.134.1080.1080/19367540_122928641630788_5062686326428532736_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c0.134.1080.1080/19380086_277275922745593_7867127244311756800_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c115.0.849.849/19366991_659090694281166_8325999772099936256_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c0.134.1080.1080/19379311_315653725545059_3992834140775907328_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c0.134.1080.1080/19228028_1922271114717734_4753461553133518848_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c0.134.1080.1080/19120980_1160240000746480_87469160761655296_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c1.0.1078.1078/19227866_691175771071524_6439474054922502144_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c0.134.1080.1080/19120921_1595441330497788_794240913042833408_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c0.134.1080.1080/19051798_424181381298309_6681973144019795968_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c0.133.1064.1064/18950206_244905959326279_5246939813541576704_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c181.0.718.718/18949563_1929132447301099_7741264675106455552_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c181.0.718.718/19051908_819817321508546_5126089917847306240_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c0.134.1080.1080/19050720_465933887132276_7408160338912215040_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/18950237_475322492816267_6783871694088962048_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c0.134.1080.1080/18948056_229053437597840_3544114060976979968_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/18888906_1699405570086903_3842204419319922688_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c1.0.1078.1078/18879500_1937470899843851_8142546755221716992_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c0.134.1080.1080/18879759_168679933666985_3066486828346376192_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/18811960_1435330249822771_7775624314690207744_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/e35/18878904_1383456025074350_5011117092192124928_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c0.134.1080.1080/18879328_139845003229953_6819310036183941120_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c0.134.1080.1080/18812818_1487530157964479_181337779844677632_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c0.134.1080.1080/18646444_120828778495333_1673384980111163392_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c181.0.718.718/18722931_429339570780698_8452973762877325312_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/e15/c236.0.607.607/18721834_2282876895270231_4425381839049326592_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c181.0.718.718/18581156_793081364180154_7270898123344445440_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/18579763_1846473359010205_1836549550735622144_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c0.100.799.799/18579739_1309644089150323_5535150286814838784_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/18644900_803011789862383_323472330867081216_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/e15/c236.0.607.607/18513282_242293499584710_8183642088936570880_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c181.0.718.718/18443755_778876932288689_5304927077537939456_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/18513570_740474016130259_2732674114606071808_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c181.0.718.718/18445014_1837839636468639_4001862363895562240_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/e15/c236.0.607.607/18444979_1863542740575981_1158740966249594880_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/18444998_214151982424585_3775194438351978496_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c0.116.931.931/18513001_396073904125872_5843670178667167744_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c0.135.1080.1080/18514220_121775838393329_2985143600913842176_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c181.0.718.718/18382033_203233693520280_8881924675148447744_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/18380596_726120947591687_7070335863724441600_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/18443467_303834576711429_7043604090353352704_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c0.135.1080.1080/18300037_1518335711519518_8471715977954852864_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/18300145_305448413217811_8779195113332015104_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c0.135.1080.1080/18299562_120279065204542_3955333617378394112_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c181.0.718.718/18253042_1800590116621640_3148834670104281088_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/18298832_306619286441681_7099469906793463808_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c180.0.719.719/18251731_1276443969091950_3555706001615224832_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/18251471_1739403466351116_3768018643217022976_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c118.0.844.844/18252619_284572418661933_3616402488032231424_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c135.0.810.810/18161919_210984256058874_5231420937059958784_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c0.13.853.853/18252136_1936058956625987_9200913558374711296_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c0.135.1080.1080/18251820_1379594525440623_6346463737808945152_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c180.0.719.719/18298874_1829094617123219_749883096400134144_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/c0.135.1080.1080/18162195_234345560378738_4419076603720499200_n.jpg',
    'https://scontent-ams3-1.cdninstagram.com/t51.2885-15/s640x640/e15/c0.90.720.720/18161560_1359215034125772_1634912424010186752_n.jpg'
];

?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            GET params:
        </div>
    </div>
    <div class="row">
        <div class="col-sm-2">
            width = <?=$width?>
        </div>
        <div class="col-sm-2">
            margin = <?=$margin?>
        </div>
        <div class="col-sm-2">
            margin_bottom = <?=$margin_bottom?>
        </div>
        <div class="col-sm-2">
            font_size = <?=$font_size?>
        </div>
    </div>
</div>


<link rel="stylesheet" href="https://templates.frisbuy.ru/css/normalize.css">
<link rel="stylesheet" href="https://templates.frisbuy.ru/css/owl.carousel.css">
<link rel="stylesheet" href="https://templates.frisbuy.ru/css/tooltipster/tooltipster.bundle.css">
<link rel="stylesheet" href="https://templates.frisbuy.ru/css/template_styles.css">



<style type="text/css">
    /**
     * Базовый контейнер
     */
    .fotoshmoto-container{
        margin-top: 30px;
        margin-bottom: 20px;
        font-size: 16px;
    }
    .fotoshmoto-header{
        margin-top: 0px;
        text-align: center;
        margin-bottom: 10px;
    }
    .fotoshmoto-heading{
        font-size: 1.5em;
    }
    .fotoshmoto-heading a{
        display: inline-block;
        padding: 3px 10px;
        font-size: 0.8em;
        border: 1px solid #000000;
        margin-left: 10px;
        text-decoration: none;
        transition: 0.2s all linear;
    }
    .fotoshmoto-heading a:hover{
        background-color: #000;
        color: #fff;
    }
    .fotoshmoto-description{
        color: #4b4b4b;
        margin-top: 5px;
    }


    /**
     * scrollbar
     */
    .fotoshmoto-list::-webkit-scrollbar {
        width: 2px;
        height: 8px;
    }
    .fotoshmoto-list::-webkit-scrollbar-button {
        width: 0px;
        height: 0px;
    }
    .fotoshmoto-list::-webkit-scrollbar-thumb {
        background: #000;
        border: 0px none transparent;
        border-radius: 2px;
    }
    .fotoshmoto-list::-webkit-scrollbar-thumb:hover {
        background: #333333;
    }
    .fotoshmoto-list::-webkit-scrollbar-thumb:active {
        background: #333333;
    }
    .fotoshmoto-list::-webkit-scrollbar-track {
        /*background: #efefef;*/
        /*border: 0px none transparent;*/
        background: transparent;
        border: 0px none transparent;
        border-bottom: 1px solid #717171;
    }
    .fotoshmoto-list::-webkit-scrollbar-track:hover {
        background: transparent;
    }
    .fotoshmoto-list::-webkit-scrollbar-track:active {
        background:  transparent;
    }
    .fotoshmoto-list::-webkit-scrollbar-corner {
        background: transparent;
    }


    /**
     * Кнопка
     */
    .fotoshmoto-btn{
        outline: none;
        position: relative;
        z-index: 3;
        background: transparent;
        color: #000;
        font-size: 14px;
        border-color: #000;
        border-style: solid;
        border-width: 1px;
        padding: 10px 40px;
        text-transform: uppercase;
        transition: all 0.2s linear;
    }
    .fotoshmoto-btn:hover {
        color: white;
        background: #000;
        border-color: #000;
    }
    .fotoshmoto-container.fotoshmoto-responsive .fotoshmoto-btn{
        display: none;
    }

    /**
     * powered-by
     */
    .fotoshmoto-powered-by{
        display: block;
        color: #777 !important;
        font-size: 12px;
        text-align: right;
        padding: 3px 0px;
    }
    .fotoshmoto-powered-by img{
        width: 55px;
        margin-top: -2px;
    }


    /**
     * list
     */
    .fotoshmoto-container .fotoshmoto-wrap{
        position: relative;
    }
    .fotoshmoto-list{
        padding: 0 !important;
        margin-top: 0 !important;
        margin-bottom: 0px !important;
        margin-right: 0 !important;
        list-style: none !important;
        margin-left: -<?=$margin?> !important;
        overflow: hidden;
        position: relative;
        z-index: 10;
    }
    .fotoshmoto-container.fotoshmoto-responsive .fotoshmoto-list{
        overflow: auto;
        white-space: nowrap;
        margin-left: 0 !important;
    }
    .fotoshmoto-container.fotoshmoto-responsive.fotoshmoto-slider .fotoshmoto-list{
        overflow: hidden;
    }
    .fotoshmoto-container.fotoshmoto-responsive.fotoshmoto-slider .fotoshmoto-wrap{
        padding: 0 32px !important;
    }
    .fotoshmoto-container:not(.fotoshmoto-responsive) .fotoshmoto-list{
        display: block !important;
    }

    /* item */
    .fotoshmoto-item{
        padding: 0 !important;
        margin: 0 !important;
        list-style: none !important;
        width: <?=$width?>;
        min-width: 100px !important;
        margin-bottom: <?=$margin_bottom?> !important;
        float: left;
        position: relative;
        font-size: <?=$font_size?>;
    }
    @media (max-width: 768px) {
        .fotoshmoto-item{
            width: 50%;
            font-size: calc(<?=$font_size?> - 5px);
        }
    }
    .fotoshmoto-item-container{
        height: 0;
        padding-bottom: calc(100% - <?=$margin?>) !important;
        margin-left: <?=$margin?> !important;
        overflow: hidden;
    }
    .fotoshmoto-item-content{
        height: 100%;
        width: calc(100% - <?=$margin?>);
        position: absolute;
        overflow: hidden;
    }
    .fotoshmoto-container.fotoshmoto-responsive .fotoshmoto-item{
        float: none;
        display: inline-block;
    }
    .fotoshmoto-container.fotoshmoto-responsive .fotoshmoto-list > li:first-child .fotoshmoto-item-container{
        margin-left: 0 !important;
    }
    .fotoshmoto-container.fotoshmoto-responsive .fotoshmoto-list > li:first-child .fotoshmoto-item-container .fotoshmoto-item-content{
        width: 100% !important;
    }

    /* video */
    .fotoshmoto-item-video-icon{
        display: none !important;
        position: absolute;
        left: 0;
        top: 0;
        opacity: 0.8;
        width: 32px !important;
        height: 32px !important;
    }
    .fotoshmoto-item.fotoshmoto-video .fotoshmoto-item-video-icon{
        display: block !important;
    }

    /* image */
    .fotoshmoto-item-image{
        width: 100% !important;
        height: 100% !important;
        object-fit: cover;
        -webkit-object-fit: cover;
    }

    /* cover */
    .fotoshmoto-item-cover-container{
        position: absolute;
        top: 0px;
        left: 0px;
        right: 0px;
        bottom: 0px;
        opacity: 0;
        margin-top: 100%;
        transition: opacity 0.5s, margin 0.5s;
        -webkit-transition: opacity 0.5s, margin 0.5s;
    }
    .fotoshmoto-item-cover-content{
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        -webkit-box-sizing: border-box;
        background: rgba(0, 0, 0, 0.4);
        color: white;
        text-align: center;
        padding: 10% 11%;
        position: absolute;
        left: 0;
        top: 0;
        height: 100%;
        width: 100%;
        font-size: 1em;
    }
    .fotoshmoto-item-cover-social{
        margin-top: 0.4em;
        font-weight: normal;
        overflow: hidden;
        font-size: 0.7em;
        white-space: nowrap;
        margin-left: -9px;
        margin-right: -9px;
    }
    .fotoshmoto-item-cover-social > div {
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        -webkit-box-sizing: border-box;
        white-space: nowrap;
        display: inline-block;
        text-align: left;
    }
    .fotoshmoto-item-cover-social > div > img {
        min-width: 12px !important;
        width: auto !important;
        display: inline-block !important;
        vertical-align: middle;
        height: <?=$font_size?>!important;
        border: 0;
    }
    .fotoshmoto-item-cover-social > div:first-child {
        padding-right: 10px;
    }
    .fotoshmoto-item-cover-social > div:last-child {
        text-align: left;
    }
    .fotoshmoto-item-cover-heading{
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
    }
    .fotoshmoto-item-cover-basket, .fotoshmoto-item-cover-zoom{
        width: 30% !important;
        opacity: 0.75;
        position: absolute;
        left: 50%;
        margin-left: -15%;
        top: 60%;
    }
    .fotoshmoto-item:not(.fotoshmoto-zoom) .fotoshmoto-item-cover-zoom{
        display: none;
    }
    .fotoshmoto-item.fotoshmoto-zoom .fotoshmoto-item-cover-basket{
        display: none;
    }
    .fotoshmoto-item-content:hover .fotoshmoto-item-cover-container{
        margin-top: 0%;
        opacity: 1;
    }
    @media (max-width: 768px) {
        .fotoshmoto-item-cover-container{
            position: static;
            opacity: 1;
            margin-top: 0;
        }
        .fotoshmoto-item-cover-content {
            bottom: 0;
            height: inherit;
            padding: 1%;
        }
        .fotoshmoto-item-cover-content {
            bottom: 0;
            height: auto;
            padding: 4% 5%;
            top: initial;
        }
        .fotoshmoto-item-cover-social {
            display: none;
        }
        .fotoshmoto-item-cover-basket{
            display: none !important;
        }
        .fotoshmoto-item-cover-zoom{
            display: none !important;
        }
    }


    /**
     * fotoshmoto nav
     */
    .fotoshmoto-nav{
        position: absolute;
        width: 100%;
        top: 0;
        left: 0;
        height: 100%;
        margin-top: 0;
        display: none;
    }
    .fotoshmoto-container.fotoshmoto-responsive.fotoshmoto-slider .fotoshmoto-nav{
        display: block;
    }
    .fotoshmoto-nav .fotoshmoto-prev, .fotoshmoto-nav .fotoshmoto-next{
        position: absolute !important;
        top: 0 !important;
        height: calc(100% - <?=$margin_bottom?> - 1px) !important;
        padding: 0 !important;
        margin: 0 !important;
        width: 30px !important;
        border-radius: 0 !important;
        display: flex !important;
        justify-content: center !important;
        align-items: center !important;
        border: 1px solid #000 !important;
        background-color: transparent !important;
        box-sizing: border-box !important;
        -ms-box-sizing: border-box !important;
        -webkit-box-sizing: border-box !important;
        transition: 0.2s all linear;
    }
    .fotoshmoto-nav .fotoshmoto-prev:hover, .fotoshmoto-nav .fotoshmoto-next:hover{
        background: #000 !important;
    }
    .fotoshmoto-nav .fotoshmoto-prev:hover svg, .fotoshmoto-nav .fotoshmoto-next:hover svg{
        -webkit-filter: invert(100%);
        filter: invert(100%);
    }
    .fotoshmoto-nav .fotoshmoto-prev{
        left: 0 !important;
    }
    .fotoshmoto-nav .fotoshmoto-next{
        right: 0 !important;
    }


    /**
     * Owl slider
     */
    .fotoshmoto-container.fotoshmoto-responsive .fotoshmoto-list.owl-carousel .fotoshmoto-item{
        width: inherit;
    }
    .fotoshmoto-container.fotoshmoto-responsive .fotoshmoto-list.owl-carousel .fotoshmoto-item-container{
        width: 100%;
    }
    .fotoshmoto-list.owl-carousel{
        padding: 0 33px 0 30px !important;
        margin-left: 0px !important;
    }

    .fotoshmoto-list.owl-carousel .fotoshmoto-item {
        min-width: inherit !important;
    }
    .fotoshmoto-list .owl-nav{
        position: absolute;
        width: 100%;
        top: 0;
        left: 0;
        height: 100%;
        margin-top: 0;
    }
    .fotoshmoto-list .owl-prev, .fotoshmoto-list .owl-next{
        position: absolute !important;
        top: 0 !important;
        height: calc(100% - <?=$margin_bottom?> - 1px) !important;
        padding: 0 !important;
        margin: 0 !important;
        width: 30px !important;
        border-radius: 0 !important;
        display: flex !important;
        justify-content: center !important;
        align-items: center !important;
        border: 1px solid #000 !important;
        background-color: transparent !important;
        box-sizing: border-box !important;
        -ms-box-sizing: border-box !important;
        -webkit-box-sizing: border-box !important;
        transition: 0.2s all linear;
    }
    .fotoshmoto-list .owl-prev:hover, .fotoshmoto-list .owl-next:hover{
        background: #000 !important;
    }
    .fotoshmoto-list.owl-carousel .owl-stage-outer {
        z-index: 10;
    }
    .fotoshmoto-list .owl-prev:hover svg, .fotoshmoto-list .owl-next:hover svg{
        -webkit-filter: invert(100%);
        filter: invert(100%);
    }
    .fotoshmoto-list .owl-prev{
        left: 0 !important;
    }
    .fotoshmoto-list .owl-next{
        right: 0 !important;
    }

</style>

<div style="background-color: #ffe8e8;">

    <div class="fotoshmoto-container fotoshmoto-responsive">

        <div class="fotoshmoto-header">

            <div class="fotoshmoto-heading">
                Заголовок Виджета
                <a href="">
                    кнопка со ссылкой
                </a>
            </div>

            <div class="fotoshmoto-description">
                fotoshmoto-container fotoshmoto-responsive
            </div>

        </div>

        <div class="fotoshmoto-powered-by">
            Сделано на
            <a href="https://www.frisbuy.ru/?utm_source=gallery&amp;utm_medium=referral&amp;utm_campaign=varvara_design" rel="nofollow" target="_blank">
                <img src="/app/media/images/frisbuy_logo_gray.svg" alt="Frisbuy">
            </a>
        </div>
        <div class="fotoshmoto-wrap">
            <ul class="fotoshmoto-list">
                <? foreach ($images as $i => $img):
                     $class_zoom = ($i%2) ? 'fotoshmoto-zoom' : '';
                     $class_video = ($i%3) ? 'fotoshmoto-video' : '';
        ?><li data-index="<?=$i?>" class="fotoshmoto-item <?=$class_zoom?> <?=$class_video?>">
        <div class="fotoshmoto-item-container frisbuy_has_link">
            <div class="fotoshmoto-item-content">
                <img class="fotoshmoto-item-image" src="<?=$img?>" alt="">
                <img src="//www.frisbuy.ru/fswidget/img/objects/play.svg" class="fotoshmoto-item-video-icon">
                <div class="fotoshmoto-item-cover-container">
                    <div class="fotoshmoto-item-cover-content">
                        <div class="fotoshmoto-item-cover-heading">
                            @varvara_design
                        </div>
                        <div class="fotoshmoto-item-cover-social">
                            <div>
                                <img src="//www.frisbuy.ru/fswidget/img/objects/favorite.svg">
                                999k
                            </div>
                            <div>
                                <img src="//www.frisbuy.ru/fswidget/img/objects/comment-bubble.svg">
                                999m
                            </div>
                        </div>
                        <img class="fotoshmoto-item-cover-basket" src="/app/media/images/bag2.svg">
                        <img class="fotoshmoto-item-cover-zoom" src="/app/media/images/zoom.svg">
                    </div>
                </div>
            </div>
        </div>
        </li><?
                endforeach;?>
            </ul>
            <div class="fotoshmoto-nav">
                <div class="fotoshmoto-prev">
                    <svg width="13px" height="30px" viewBox="0 0 13 30" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><polyline id="fotoshmoto-left-arrow" stroke="#000000" transform="translate(6.336426, 14.830078) scale(1, -1) translate(-6.336426, -14.830078) " points="11.59375 0.33984375 1.07910156 14.6601563 11.59375 29.3203125"></polyline></g>
                    </svg>
                </div>
                <div class="fotoshmoto-next">
                    <svg width="13px" height="30px" viewBox="0 0 13 30" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><polyline id="fotoshmoto-right-arrow" stroke="#000000" transform="translate(6.336426, 14.830078) scale(-1, -1) translate(-6.336426, -14.830078) " points="11.59375 0.33984375 1.07910156 14.6601563 11.59375 29.3203125"></polyline></g>
                    </svg>
                </div>
            </div>
        </div>
        <button type="submit" id="fotoshmoto-load-more" class="fotoshmoto-btn">Ещё</button>
    </div>

    <div class="fotoshmoto-container fotoshmoto-responsive fotoshmoto-slider">

        <div class="fotoshmoto-header">

            <div class="fotoshmoto-heading">
                Заголовок Виджета
                <a href="">
                    кнопка со ссылкой
                </a>
            </div>

            <div class="fotoshmoto-description">
                fotoshmoto-container fotoshmoto-responsive fotoshmoto-slider
            </div>

        </div>

        <div class="fotoshmoto-powered-by">
            Сделано на
            <a href="https://www.frisbuy.ru/?utm_source=gallery&amp;utm_medium=referral&amp;utm_campaign=varvara_design" rel="nofollow" target="_blank">
                <img src="/app/media/images/frisbuy_logo_gray.svg" alt="Frisbuy">
            </a>
        </div>
        <div class="fotoshmoto-wrap">
            <ul class="fotoshmoto-list">
            <? foreach ($images as $i => $img):
                $class_zoom = ($i%2) ? 'fotoshmoto-zoom' : '';
                $class_video = ($i%3) ? 'fotoshmoto-video' : '';
                ?><li data-index="<?=$i?>" class="fotoshmoto-item <?=$class_zoom?> <?=$class_video?>">
                <div class="fotoshmoto-item-container frisbuy_has_link">
                    <div class="fotoshmoto-item-content">
                        <img class="fotoshmoto-item-image" src="<?=$img?>" alt="">
                        <img src="//www.frisbuy.ru/fswidget/img/objects/play.svg" class="fotoshmoto-item-video-icon">
                        <div class="fotoshmoto-item-cover-container">
                            <div class="fotoshmoto-item-cover-content">
                                <div class="fotoshmoto-item-cover-heading">
                                    @varvara_design
                                </div>
                                <div class="fotoshmoto-item-cover-social">
                                    <div>
                                        <img src="//www.frisbuy.ru/fswidget/img/objects/favorite.svg">
                                        999k
                                    </div>
                                    <div>
                                        <img src="//www.frisbuy.ru/fswidget/img/objects/comment-bubble.svg">
                                        999m
                                    </div>
                                </div>
                                <img class="fotoshmoto-item-cover-basket" src="/app/media/images/bag2.svg">
                                <img class="fotoshmoto-item-cover-zoom" src="/app/media/images/zoom.svg">
                            </div>
                        </div>
                    </div>
                </div>
                </li><?
            endforeach;?>
        </ul>
            <div class="fotoshmoto-nav">
                <div class="fotoshmoto-prev">
                    <svg width="13px" height="30px" viewBox="0 0 13 30" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><polyline id="fotoshmoto-left-arrow" stroke="#000000" transform="translate(6.336426, 14.830078) scale(1, -1) translate(-6.336426, -14.830078) " points="11.59375 0.33984375 1.07910156 14.6601563 11.59375 29.3203125"></polyline></g>
                    </svg>
                </div>
                <div class="fotoshmoto-next">
                    <svg width="13px" height="30px" viewBox="0 0 13 30" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><polyline id="fotoshmoto-right-arrow" stroke="#000000" transform="translate(6.336426, 14.830078) scale(-1, -1) translate(-6.336426, -14.830078) " points="11.59375 0.33984375 1.07910156 14.6601563 11.59375 29.3203125"></polyline></g>
                    </svg>
                </div>
            </div>
        </div>
        <button type="submit" id="fotoshmoto-load-more" class="fotoshmoto-btn">Ещё</button>
    </div>

    <div class="fotoshmoto-container fotoshmoto-responsive">

        <div class="fotoshmoto-header">

            <div class="fotoshmoto-heading">
                Заголовок Виджета
                <a href="">
                    кнопка со ссылкой
                </a>
            </div>

            <div class="fotoshmoto-description">
                OWL slider + fotoshmoto-responsive
            </div>

        </div>

        <div class="fotoshmoto-powered-by">
            Сделано на
            <a href="https://www.frisbuy.ru/?utm_source=gallery&amp;utm_medium=referral&amp;utm_campaign=varvara_design" rel="nofollow" target="_blank">
                <img src="/app/media/images/frisbuy_logo_gray.svg" alt="Frisbuy">
            </a>
        </div>
        <div class="fotoshmoto-wrap">
            <ul class="fotoshmoto-list owl-carousel owl-theme">
            <? foreach ($images as $i => $img):
                 $class_zoom = ($i%2) ? 'fotoshmoto-zoom' : '';
                 $class_video = ($i%3) ? 'fotoshmoto-video' : '';
    ?><li data-index="<?=$i?>" class="fotoshmoto-item <?=$class_zoom?> <?=$class_video?>">
    <div class="fotoshmoto-item-container frisbuy_has_link">
        <div class="fotoshmoto-item-content">
            <img class="fotoshmoto-item-image" src="<?=$img?>" alt="">
            <img src="//www.frisbuy.ru/fswidget/img/objects/play.svg" class="fotoshmoto-item-video-icon">
            <div class="fotoshmoto-item-cover-container">
                <div class="fotoshmoto-item-cover-content">
                    <div class="fotoshmoto-item-cover-heading">
                        @varvara_design
                    </div>
                    <div class="fotoshmoto-item-cover-social">
                        <div>
                            <img src="//www.frisbuy.ru/fswidget/img/objects/favorite.svg">
                            999k
                        </div>
                        <div>
                            <img src="//www.frisbuy.ru/fswidget/img/objects/comment-bubble.svg">
                            999m
                        </div>
                    </div>
                    <img class="fotoshmoto-item-cover-basket" src="/app/media/images/bag2.svg">
                    <img class="fotoshmoto-item-cover-zoom" src="/app/media/images/zoom.svg">
                </div>
            </div>
        </div>
    </div>
    </li><?
            endforeach;?>
        </ul>
            <div class="fotoshmoto-nav">
                <div class="fotoshmoto-prev">
                    <svg width="13px" height="30px" viewBox="0 0 13 30" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><polyline id="fotoshmoto-left-arrow" stroke="#000000" transform="translate(6.336426, 14.830078) scale(1, -1) translate(-6.336426, -14.830078) " points="11.59375 0.33984375 1.07910156 14.6601563 11.59375 29.3203125"></polyline></g>
                    </svg>
                </div>
                <div class="fotoshmoto-next">
                    <svg width="13px" height="30px" viewBox="0 0 13 30" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><polyline id="fotoshmoto-right-arrow" stroke="#000000" transform="translate(6.336426, 14.830078) scale(-1, -1) translate(-6.336426, -14.830078) " points="11.59375 0.33984375 1.07910156 14.6601563 11.59375 29.3203125"></polyline></g>
                    </svg>
                </div>
            </div>
        </div>
        <button type="submit" id="fotoshmoto-load-more" class="fotoshmoto-btn">Ещё</button>
    </div>

    <div class="fotoshmoto-container">

        <div class="fotoshmoto-header">

            <div class="fotoshmoto-heading">
                Заголовок Виджета
                <a href="">
                    кнопка со ссылкой
                </a>
            </div>

            <div class="fotoshmoto-description">
                Базовый
            </div>

        </div>

        <div class="fotoshmoto-powered-by">
            Сделано на
            <a href="https://www.frisbuy.ru/?utm_source=gallery&amp;utm_medium=referral&amp;utm_campaign=varvara_design" rel="nofollow" target="_blank">
                <img src="/app/media/images/frisbuy_logo_gray.svg" alt="Frisbuy">
            </a>
        </div>
        <div class="fotoshmoto-wrap">
            <ul class="fotoshmoto-list">
            <? foreach ($images as $i => $img):?>

                <? $class_zoom = ($i%2) ? 'fotoshmoto-zoom' : '';?>

                <? $class_video = ($i%3) ? 'fotoshmoto-video' : '';?>
                <li data-index="<?=$i?>" class="fotoshmoto-item <?=$class_zoom?> <?=$class_video?>">
                    <div class="fotoshmoto-item-container frisbuy_has_link">
                        <div class="fotoshmoto-item-content">
                            <img class="fotoshmoto-item-image" src="<?=$img?>" alt="">
                            <img src="//www.frisbuy.ru/fswidget/img/objects/play.svg" class="fotoshmoto-item-video-icon">
                            <div class="fotoshmoto-item-cover-container">
                                <div class="fotoshmoto-item-cover-content">
                                    <div class="fotoshmoto-item-cover-heading">
                                        @varvara_design
                                    </div>
                                    <div class="fotoshmoto-item-cover-social">
                                        <div>
                                            <img src="//www.frisbuy.ru/fswidget/img/objects/favorite.svg">
                                            999k
                                        </div>
                                        <div>
                                            <img src="//www.frisbuy.ru/fswidget/img/objects/comment-bubble.svg">
                                            999m
                                        </div>
                                    </div>
                                    <img class="fotoshmoto-item-cover-basket" src="/app/media/images/bag2.svg">
                                    <img class="fotoshmoto-item-cover-zoom" src="/app/media/images/zoom.svg">
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            <? endforeach;?>
        </ul>
            <div class="fotoshmoto-nav">
                <div class="fotoshmoto-prev">
                    <svg width="13px" height="30px" viewBox="0 0 13 30" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><polyline id="fotoshmoto-left-arrow" stroke="#000000" transform="translate(6.336426, 14.830078) scale(1, -1) translate(-6.336426, -14.830078) " points="11.59375 0.33984375 1.07910156 14.6601563 11.59375 29.3203125"></polyline></g>
                    </svg>
                </div>
                <div class="fotoshmoto-next">
                    <svg width="13px" height="30px" viewBox="0 0 13 30" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><polyline id="fotoshmoto-right-arrow" stroke="#000000" transform="translate(6.336426, 14.830078) scale(-1, -1) translate(-6.336426, -14.830078) " points="11.59375 0.33984375 1.07910156 14.6601563 11.59375 29.3203125"></polyline></g>
                    </svg>
                </div>
            </div>
        </div>
        <button type="submit" id="fotoshmoto-load-more" class="fotoshmoto-btn">Ещё</button>
    </div>
</div>

<div style="width: 330px; background-color: #ffe8e8;">

    <div class="fotoshmoto-container fotoshmoto-responsive">

        <div class="fotoshmoto-header">

            <div class="fotoshmoto-heading">
                Заголовок Виджета
                <a href="">
                    кнопка со ссылкой
                </a>
            </div>

            <div class="fotoshmoto-description">
                fotoshmoto-container fotoshmoto-responsive
            </div>

        </div>

        <div class="fotoshmoto-powered-by">
            Сделано на
            <a href="https://www.frisbuy.ru/?utm_source=gallery&amp;utm_medium=referral&amp;utm_campaign=varvara_design" rel="nofollow" target="_blank">
                <img src="/app/media/images/frisbuy_logo_gray.svg" alt="Frisbuy">
            </a>
        </div>
        <div class="fotoshmoto-wrap">
            <ul class="fotoshmoto-list">
            <? foreach ($images as $i => $img):
                $class_zoom = ($i%2) ? 'fotoshmoto-zoom' : '';
                $class_video = ($i%3) ? 'fotoshmoto-video' : '';
                ?><li data-index="<?=$i?>" class="fotoshmoto-item <?=$class_zoom?> <?=$class_video?>">
                <div class="fotoshmoto-item-container frisbuy_has_link">
                    <div class="fotoshmoto-item-content">
                        <img class="fotoshmoto-item-image" src="<?=$img?>" alt="">
                        <img src="//www.frisbuy.ru/fswidget/img/objects/play.svg" class="fotoshmoto-item-video-icon">
                        <div class="fotoshmoto-item-cover-container">
                            <div class="fotoshmoto-item-cover-content">
                                <div class="fotoshmoto-item-cover-heading">
                                    @varvara_design
                                </div>
                                <div class="fotoshmoto-item-cover-social">
                                    <div>
                                        <img src="//www.frisbuy.ru/fswidget/img/objects/favorite.svg">
                                        999k
                                    </div>
                                    <div>
                                        <img src="//www.frisbuy.ru/fswidget/img/objects/comment-bubble.svg">
                                        999m
                                    </div>
                                </div>
                                <img class="fotoshmoto-item-cover-basket" src="/app/media/images/bag2.svg">
                                <img class="fotoshmoto-item-cover-zoom" src="/app/media/images/zoom.svg">
                            </div>
                        </div>
                    </div>
                </div>
                </li><?
            endforeach;?>
        </ul>
            <div class="fotoshmoto-nav">
                <div class="fotoshmoto-prev">
                    <svg width="13px" height="30px" viewBox="0 0 13 30" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><polyline id="fotoshmoto-left-arrow" stroke="#000000" transform="translate(6.336426, 14.830078) scale(1, -1) translate(-6.336426, -14.830078) " points="11.59375 0.33984375 1.07910156 14.6601563 11.59375 29.3203125"></polyline></g>
                    </svg>
                </div>
                <div class="fotoshmoto-next">
                    <svg width="13px" height="30px" viewBox="0 0 13 30" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><polyline id="fotoshmoto-right-arrow" stroke="#000000" transform="translate(6.336426, 14.830078) scale(-1, -1) translate(-6.336426, -14.830078) " points="11.59375 0.33984375 1.07910156 14.6601563 11.59375 29.3203125"></polyline></g>
                    </svg>
                </div>
            </div>
        </div>
        <button type="submit" id="fotoshmoto-load-more" class="fotoshmoto-btn">Ещё</button>
    </div>

    <div class="fotoshmoto-container fotoshmoto-responsive fotoshmoto-slider">

        <div class="fotoshmoto-header">

            <div class="fotoshmoto-heading">
                Заголовок Виджета
                <a href="">
                    кнопка со ссылкой
                </a>
            </div>

            <div class="fotoshmoto-description">
                fotoshmoto-container fotoshmoto-responsive fotoshmoto-slider
            </div>

        </div>

        <div class="fotoshmoto-powered-by">
            Сделано на
            <a href="https://www.frisbuy.ru/?utm_source=gallery&amp;utm_medium=referral&amp;utm_campaign=varvara_design" rel="nofollow" target="_blank">
                <img src="/app/media/images/frisbuy_logo_gray.svg" alt="Frisbuy">
            </a>
        </div>
        <div class="fotoshmoto-wrap">
            <ul class="fotoshmoto-list">
            <? foreach ($images as $i => $img):
                $class_zoom = ($i%2) ? 'fotoshmoto-zoom' : '';
                $class_video = ($i%3) ? 'fotoshmoto-video' : '';
                ?><li data-index="<?=$i?>" class="fotoshmoto-item <?=$class_zoom?> <?=$class_video?>">
                <div class="fotoshmoto-item-container frisbuy_has_link">
                    <div class="fotoshmoto-item-content">
                        <img class="fotoshmoto-item-image" src="<?=$img?>" alt="">
                        <img src="//www.frisbuy.ru/fswidget/img/objects/play.svg" class="fotoshmoto-item-video-icon">
                        <div class="fotoshmoto-item-cover-container">
                            <div class="fotoshmoto-item-cover-content">
                                <div class="fotoshmoto-item-cover-heading">
                                    @varvara_design
                                </div>
                                <div class="fotoshmoto-item-cover-social">
                                    <div>
                                        <img src="//www.frisbuy.ru/fswidget/img/objects/favorite.svg">
                                        999k
                                    </div>
                                    <div>
                                        <img src="//www.frisbuy.ru/fswidget/img/objects/comment-bubble.svg">
                                        999m
                                    </div>
                                </div>
                                <img class="fotoshmoto-item-cover-basket" src="/app/media/images/bag2.svg">
                                <img class="fotoshmoto-item-cover-zoom" src="/app/media/images/zoom.svg">
                            </div>
                        </div>
                    </div>
                </div>
                </li><?
            endforeach;?>
        </ul>
            <div class="fotoshmoto-nav">
                <div class="fotoshmoto-prev">
                    <svg width="13px" height="30px" viewBox="0 0 13 30" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><polyline id="fotoshmoto-left-arrow" stroke="#000000" transform="translate(6.336426, 14.830078) scale(1, -1) translate(-6.336426, -14.830078) " points="11.59375 0.33984375 1.07910156 14.6601563 11.59375 29.3203125"></polyline></g>
                    </svg>
                </div>
                <div class="fotoshmoto-next">
                    <svg width="13px" height="30px" viewBox="0 0 13 30" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><polyline id="fotoshmoto-right-arrow" stroke="#000000" transform="translate(6.336426, 14.830078) scale(-1, -1) translate(-6.336426, -14.830078) " points="11.59375 0.33984375 1.07910156 14.6601563 11.59375 29.3203125"></polyline></g>
                    </svg>
                </div>
            </div>
        </div>
        <button type="submit" id="fotoshmoto-load-more" class="fotoshmoto-btn">Ещё</button>
    </div>

    <div class="fotoshmoto-container fotoshmoto-responsive">

        <div class="fotoshmoto-header">

            <div class="fotoshmoto-heading">
                Заголовок Виджета
                <a href="">
                    кнопка со ссылкой
                </a>
            </div>

            <div class="fotoshmoto-description">
                OWL slider + fotoshmoto-responsive
            </div>

        </div>

        <div class="fotoshmoto-powered-by">
            Сделано на
            <a href="https://www.frisbuy.ru/?utm_source=gallery&amp;utm_medium=referral&amp;utm_campaign=varvara_design" rel="nofollow" target="_blank">
                <img src="/app/media/images/frisbuy_logo_gray.svg" alt="Frisbuy">
            </a>
        </div>
        <div class="fotoshmoto-wrap">
            <ul class="fotoshmoto-list owl-carousel owl-theme">
            <? foreach ($images as $i => $img):
                $class_zoom = ($i%2) ? 'fotoshmoto-zoom' : '';
                $class_video = ($i%3) ? 'fotoshmoto-video' : '';
                ?><li data-index="<?=$i?>" class="fotoshmoto-item <?=$class_zoom?> <?=$class_video?>">
                <div class="fotoshmoto-item-container frisbuy_has_link">
                    <div class="fotoshmoto-item-content">
                        <img class="fotoshmoto-item-image" src="<?=$img?>" alt="">
                        <img src="//www.frisbuy.ru/fswidget/img/objects/play.svg" class="fotoshmoto-item-video-icon">
                        <div class="fotoshmoto-item-cover-container">
                            <div class="fotoshmoto-item-cover-content">
                                <div class="fotoshmoto-item-cover-heading">
                                    @varvara_design
                                </div>
                                <div class="fotoshmoto-item-cover-social">
                                    <div>
                                        <img src="//www.frisbuy.ru/fswidget/img/objects/favorite.svg">
                                        999k
                                    </div>
                                    <div>
                                        <img src="//www.frisbuy.ru/fswidget/img/objects/comment-bubble.svg">
                                        999m
                                    </div>
                                </div>
                                <img class="fotoshmoto-item-cover-basket" src="/app/media/images/bag2.svg">
                                <img class="fotoshmoto-item-cover-zoom" src="/app/media/images/zoom.svg">
                            </div>
                        </div>
                    </div>
                </div>
                </li><?
            endforeach;?>
        </ul>
            <div class="fotoshmoto-nav">
                <div class="fotoshmoto-prev">
                    <svg width="13px" height="30px" viewBox="0 0 13 30" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><polyline id="fotoshmoto-left-arrow" stroke="#000000" transform="translate(6.336426, 14.830078) scale(1, -1) translate(-6.336426, -14.830078) " points="11.59375 0.33984375 1.07910156 14.6601563 11.59375 29.3203125"></polyline></g>
                    </svg>
                </div>
                <div class="fotoshmoto-next">
                    <svg width="13px" height="30px" viewBox="0 0 13 30" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><polyline id="fotoshmoto-right-arrow" stroke="#000000" transform="translate(6.336426, 14.830078) scale(-1, -1) translate(-6.336426, -14.830078) " points="11.59375 0.33984375 1.07910156 14.6601563 11.59375 29.3203125"></polyline></g>
                    </svg>
                </div>
            </div>
        </div>
        <button type="submit" id="fotoshmoto-load-more" class="fotoshmoto-btn">Ещё</button>
    </div>

    <div class="fotoshmoto-container">

        <div class="fotoshmoto-header">

            <div class="fotoshmoto-heading">
                Заголовок Виджета
                <a href="">
                    кнопка со ссылкой
                </a>
            </div>

            <div class="fotoshmoto-description">
                Базовый
            </div>

        </div>

        <div class="fotoshmoto-powered-by">
            Сделано на
            <a href="https://www.frisbuy.ru/?utm_source=gallery&amp;utm_medium=referral&amp;utm_campaign=varvara_design" rel="nofollow" target="_blank">
                <img src="/app/media/images/frisbuy_logo_gray.svg" alt="Frisbuy">
            </a>
        </div>
        <div class="fotoshmoto-wrap">
            <ul class="fotoshmoto-list">
            <? foreach ($images as $i => $img):?>

                <? $class_zoom = ($i%2) ? 'fotoshmoto-zoom' : '';?>

                <? $class_video = ($i%3) ? 'fotoshmoto-video' : '';?>
                <li data-index="<?=$i?>" class="fotoshmoto-item <?=$class_zoom?> <?=$class_video?>">
                    <div class="fotoshmoto-item-container frisbuy_has_link">
                        <div class="fotoshmoto-item-content">
                            <img class="fotoshmoto-item-image" src="<?=$img?>" alt="">
                            <img src="//www.frisbuy.ru/fswidget/img/objects/play.svg" class="fotoshmoto-item-video-icon">
                            <div class="fotoshmoto-item-cover-container">
                                <div class="fotoshmoto-item-cover-content">
                                    <div class="fotoshmoto-item-cover-heading">
                                        @varvara_design
                                    </div>
                                    <div class="fotoshmoto-item-cover-social">
                                        <div>
                                            <img src="//www.frisbuy.ru/fswidget/img/objects/favorite.svg">
                                            999k
                                        </div>
                                        <div>
                                            <img src="//www.frisbuy.ru/fswidget/img/objects/comment-bubble.svg">
                                            999m
                                        </div>
                                    </div>
                                    <img class="fotoshmoto-item-cover-basket" src="/app/media/images/bag2.svg">
                                    <img class="fotoshmoto-item-cover-zoom" src="/app/media/images/zoom.svg">
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            <? endforeach;?>
        </ul>
            <div class="fotoshmoto-nav">
                <div class="fotoshmoto-prev">
                    <svg width="13px" height="30px" viewBox="0 0 13 30" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><polyline id="fotoshmoto-left-arrow" stroke="#000000" transform="translate(6.336426, 14.830078) scale(1, -1) translate(-6.336426, -14.830078) " points="11.59375 0.33984375 1.07910156 14.6601563 11.59375 29.3203125"></polyline></g>
                    </svg>
                </div>
                <div class="fotoshmoto-next">
                    <svg width="13px" height="30px" viewBox="0 0 13 30" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><polyline id="fotoshmoto-right-arrow" stroke="#000000" transform="translate(6.336426, 14.830078) scale(-1, -1) translate(-6.336426, -14.830078) " points="11.59375 0.33984375 1.07910156 14.6601563 11.59375 29.3203125"></polyline></g>
                    </svg>
                </div>
            </div>
        </div>
        <button type="submit" id="fotoshmoto-load-more" class="fotoshmoto-btn">Ещё</button>
    </div>
</div>

<div style="width: 680px; background-color: #ffe8e8;">

    <div class="fotoshmoto-container fotoshmoto-responsive">

        <div class="fotoshmoto-header">

            <div class="fotoshmoto-heading">
                Заголовок Виджета
                <a href="">
                    кнопка со ссылкой
                </a>
            </div>

            <div class="fotoshmoto-description">
                fotoshmoto-container fotoshmoto-responsive
            </div>

        </div>

        <div class="fotoshmoto-powered-by">
            Сделано на
            <a href="https://www.frisbuy.ru/?utm_source=gallery&amp;utm_medium=referral&amp;utm_campaign=varvara_design" rel="nofollow" target="_blank">
                <img src="/app/media/images/frisbuy_logo_gray.svg" alt="Frisbuy">
            </a>
        </div>
        <div class="fotoshmoto-wrap">
            <ul class="fotoshmoto-list">
            <? foreach ($images as $i => $img):
                $class_zoom = ($i%2) ? 'fotoshmoto-zoom' : '';
                $class_video = ($i%3) ? 'fotoshmoto-video' : '';
                ?><li data-index="<?=$i?>" class="fotoshmoto-item <?=$class_zoom?> <?=$class_video?>">
                <div class="fotoshmoto-item-container frisbuy_has_link">
                    <div class="fotoshmoto-item-content">
                        <img class="fotoshmoto-item-image" src="<?=$img?>" alt="">
                        <img src="//www.frisbuy.ru/fswidget/img/objects/play.svg" class="fotoshmoto-item-video-icon">
                        <div class="fotoshmoto-item-cover-container">
                            <div class="fotoshmoto-item-cover-content">
                                <div class="fotoshmoto-item-cover-heading">
                                    @varvara_design
                                </div>
                                <div class="fotoshmoto-item-cover-social">
                                    <div>
                                        <img src="//www.frisbuy.ru/fswidget/img/objects/favorite.svg">
                                        999k
                                    </div>
                                    <div>
                                        <img src="//www.frisbuy.ru/fswidget/img/objects/comment-bubble.svg">
                                        999m
                                    </div>
                                </div>
                                <img class="fotoshmoto-item-cover-basket" src="/app/media/images/bag2.svg">
                                <img class="fotoshmoto-item-cover-zoom" src="/app/media/images/zoom.svg">
                            </div>
                        </div>
                    </div>
                </div>
                </li><?
            endforeach;?>
        </ul>
            <div class="fotoshmoto-nav">
                <div class="fotoshmoto-prev">
                    <svg width="13px" height="30px" viewBox="0 0 13 30" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><polyline id="fotoshmoto-left-arrow" stroke="#000000" transform="translate(6.336426, 14.830078) scale(1, -1) translate(-6.336426, -14.830078) " points="11.59375 0.33984375 1.07910156 14.6601563 11.59375 29.3203125"></polyline></g>
                    </svg>
                </div>
                <div class="fotoshmoto-next">
                    <svg width="13px" height="30px" viewBox="0 0 13 30" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><polyline id="fotoshmoto-right-arrow" stroke="#000000" transform="translate(6.336426, 14.830078) scale(-1, -1) translate(-6.336426, -14.830078) " points="11.59375 0.33984375 1.07910156 14.6601563 11.59375 29.3203125"></polyline></g>
                    </svg>
                </div>
            </div>
        </div>
        <button type="submit" id="fotoshmoto-load-more" class="fotoshmoto-btn">Ещё</button>
    </div>

    <div class="fotoshmoto-container fotoshmoto-responsive fotoshmoto-slider">

        <div class="fotoshmoto-header">

            <div class="fotoshmoto-heading">
                Заголовок Виджета
                <a href="">
                    кнопка со ссылкой
                </a>
            </div>

            <div class="fotoshmoto-description">
                fotoshmoto-container fotoshmoto-responsive fotoshmoto-slider
            </div>

        </div>

        <div class="fotoshmoto-powered-by">
            Сделано на
            <a href="https://www.frisbuy.ru/?utm_source=gallery&amp;utm_medium=referral&amp;utm_campaign=varvara_design" rel="nofollow" target="_blank">
                <img src="/app/media/images/frisbuy_logo_gray.svg" alt="Frisbuy">
            </a>
        </div>
        <div class="fotoshmoto-wrap">
            <ul class="fotoshmoto-list">
            <? foreach ($images as $i => $img):
                $class_zoom = ($i%2) ? 'fotoshmoto-zoom' : '';
                $class_video = ($i%3) ? 'fotoshmoto-video' : '';
                ?><li data-index="<?=$i?>" class="fotoshmoto-item <?=$class_zoom?> <?=$class_video?>">
                <div class="fotoshmoto-item-container frisbuy_has_link">
                    <div class="fotoshmoto-item-content">
                        <img class="fotoshmoto-item-image" src="<?=$img?>" alt="">
                        <img src="//www.frisbuy.ru/fswidget/img/objects/play.svg" class="fotoshmoto-item-video-icon">
                        <div class="fotoshmoto-item-cover-container">
                            <div class="fotoshmoto-item-cover-content">
                                <div class="fotoshmoto-item-cover-heading">
                                    @varvara_design
                                </div>
                                <div class="fotoshmoto-item-cover-social">
                                    <div>
                                        <img src="//www.frisbuy.ru/fswidget/img/objects/favorite.svg">
                                        999k
                                    </div>
                                    <div>
                                        <img src="//www.frisbuy.ru/fswidget/img/objects/comment-bubble.svg">
                                        999m
                                    </div>
                                </div>
                                <img class="fotoshmoto-item-cover-basket" src="/app/media/images/bag2.svg">
                                <img class="fotoshmoto-item-cover-zoom" src="/app/media/images/zoom.svg">
                            </div>
                        </div>
                    </div>
                </div>
                </li><?
            endforeach;?>
        </ul>
            <div class="fotoshmoto-nav">
                <div class="fotoshmoto-prev">
                    <svg width="13px" height="30px" viewBox="0 0 13 30" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><polyline id="fotoshmoto-left-arrow" stroke="#000000" transform="translate(6.336426, 14.830078) scale(1, -1) translate(-6.336426, -14.830078) " points="11.59375 0.33984375 1.07910156 14.6601563 11.59375 29.3203125"></polyline></g>
                    </svg>
                </div>
                <div class="fotoshmoto-next">
                    <svg width="13px" height="30px" viewBox="0 0 13 30" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><polyline id="fotoshmoto-right-arrow" stroke="#000000" transform="translate(6.336426, 14.830078) scale(-1, -1) translate(-6.336426, -14.830078) " points="11.59375 0.33984375 1.07910156 14.6601563 11.59375 29.3203125"></polyline></g>
                    </svg>
                </div>
            </div>
        </div>
        <button type="submit" id="fotoshmoto-load-more" class="fotoshmoto-btn">Ещё</button>
    </div>

    <div class="fotoshmoto-container fotoshmoto-responsive">

        <div class="fotoshmoto-header">

            <div class="fotoshmoto-heading">
                Заголовок Виджета
                <a href="">
                    кнопка со ссылкой
                </a>
            </div>

            <div class="fotoshmoto-description">
                OWL slider + fotoshmoto-responsive
            </div>

        </div>

        <div class="fotoshmoto-powered-by">
            Сделано на
            <a href="https://www.frisbuy.ru/?utm_source=gallery&amp;utm_medium=referral&amp;utm_campaign=varvara_design" rel="nofollow" target="_blank">
                <img src="/app/media/images/frisbuy_logo_gray.svg" alt="Frisbuy">
            </a>
        </div>
        <div class="fotoshmoto-wrap">
            <ul class="fotoshmoto-list owl-carousel owl-theme">
            <? foreach ($images as $i => $img):
                $class_zoom = ($i%2) ? 'fotoshmoto-zoom' : '';
                $class_video = ($i%3) ? 'fotoshmoto-video' : '';
                ?><li data-index="<?=$i?>" class="fotoshmoto-item <?=$class_zoom?> <?=$class_video?>">
                <div class="fotoshmoto-item-container frisbuy_has_link">
                    <div class="fotoshmoto-item-content">
                        <img class="fotoshmoto-item-image" src="<?=$img?>" alt="">
                        <img src="//www.frisbuy.ru/fswidget/img/objects/play.svg" class="fotoshmoto-item-video-icon">
                        <div class="fotoshmoto-item-cover-container">
                            <div class="fotoshmoto-item-cover-content">
                                <div class="fotoshmoto-item-cover-heading">
                                    @varvara_design
                                </div>
                                <div class="fotoshmoto-item-cover-social">
                                    <div>
                                        <img src="//www.frisbuy.ru/fswidget/img/objects/favorite.svg">
                                        999k
                                    </div>
                                    <div>
                                        <img src="//www.frisbuy.ru/fswidget/img/objects/comment-bubble.svg">
                                        999m
                                    </div>
                                </div>
                                <img class="fotoshmoto-item-cover-basket" src="/app/media/images/bag2.svg">
                                <img class="fotoshmoto-item-cover-zoom" src="/app/media/images/zoom.svg">
                            </div>
                        </div>
                    </div>
                </div>
                </li><?
            endforeach;?>
        </ul>
            <div class="fotoshmoto-nav">
                <div class="fotoshmoto-prev">
                    <svg width="13px" height="30px" viewBox="0 0 13 30" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><polyline id="fotoshmoto-left-arrow" stroke="#000000" transform="translate(6.336426, 14.830078) scale(1, -1) translate(-6.336426, -14.830078) " points="11.59375 0.33984375 1.07910156 14.6601563 11.59375 29.3203125"></polyline></g>
                    </svg>
                </div>
                <div class="fotoshmoto-next">
                    <svg width="13px" height="30px" viewBox="0 0 13 30" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><polyline id="fotoshmoto-right-arrow" stroke="#000000" transform="translate(6.336426, 14.830078) scale(-1, -1) translate(-6.336426, -14.830078) " points="11.59375 0.33984375 1.07910156 14.6601563 11.59375 29.3203125"></polyline></g>
                    </svg>
                </div>
            </div>
        </div>
        <button type="submit" id="fotoshmoto-load-more" class="fotoshmoto-btn">Ещё</button>
    </div>

    <div class="fotoshmoto-container">

        <div class="fotoshmoto-header">

            <div class="fotoshmoto-heading">
                Заголовок Виджета
                <a href="">
                    кнопка со ссылкой
                </a>
            </div>

            <div class="fotoshmoto-description">
                Базовый
            </div>

        </div>

        <div class="fotoshmoto-powered-by">
            Сделано на
            <a href="https://www.frisbuy.ru/?utm_source=gallery&amp;utm_medium=referral&amp;utm_campaign=varvara_design" rel="nofollow" target="_blank">
                <img src="/app/media/images/frisbuy_logo_gray.svg" alt="Frisbuy">
            </a>
        </div>
        <div class="fotoshmoto-wrap">
            <ul class="fotoshmoto-list">
            <? foreach ($images as $i => $img):?>

                <? $class_zoom = ($i%2) ? 'fotoshmoto-zoom' : '';?>

                <? $class_video = ($i%3) ? 'fotoshmoto-video' : '';?>
                <li data-index="<?=$i?>" class="fotoshmoto-item <?=$class_zoom?> <?=$class_video?>">
                    <div class="fotoshmoto-item-container frisbuy_has_link">
                        <div class="fotoshmoto-item-content">
                            <img class="fotoshmoto-item-image" src="<?=$img?>" alt="">
                            <img src="//www.frisbuy.ru/fswidget/img/objects/play.svg" class="fotoshmoto-item-video-icon">
                            <div class="fotoshmoto-item-cover-container">
                                <div class="fotoshmoto-item-cover-content">
                                    <div class="fotoshmoto-item-cover-heading">
                                        @varvara_design
                                    </div>
                                    <div class="fotoshmoto-item-cover-social">
                                        <div>
                                            <img src="//www.frisbuy.ru/fswidget/img/objects/favorite.svg">
                                            999k
                                        </div>
                                        <div>
                                            <img src="//www.frisbuy.ru/fswidget/img/objects/comment-bubble.svg">
                                            999m
                                        </div>
                                    </div>
                                    <img class="fotoshmoto-item-cover-basket" src="/app/media/images/bag2.svg">
                                    <img class="fotoshmoto-item-cover-zoom" src="/app/media/images/zoom.svg">
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            <? endforeach;?>
        </ul>
            <div class="fotoshmoto-nav">
                <div class="fotoshmoto-prev">
                    <svg width="13px" height="30px" viewBox="0 0 13 30" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><polyline id="fotoshmoto-left-arrow" stroke="#000000" transform="translate(6.336426, 14.830078) scale(1, -1) translate(-6.336426, -14.830078) " points="11.59375 0.33984375 1.07910156 14.6601563 11.59375 29.3203125"></polyline></g>
                    </svg>
                </div>
                <div class="fotoshmoto-next">
                    <svg width="13px" height="30px" viewBox="0 0 13 30" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><polyline id="fotoshmoto-right-arrow" stroke="#000000" transform="translate(6.336426, 14.830078) scale(-1, -1) translate(-6.336426, -14.830078) " points="11.59375 0.33984375 1.07910156 14.6601563 11.59375 29.3203125"></polyline></g>
                    </svg>
                </div>
            </div>
        </div>
        <button type="submit" id="fotoshmoto-load-more" class="fotoshmoto-btn">Ещё</button>
    </div>
</div>


<script>
    $('.fotoshmoto-responsive .owl-carousel').owlCarousel({
        singleItem: false,
        autoplay: true,
        autoplayTimeout: 3000,
        nav: true,
        navText: [
            '<svg width="13px" height="30px" viewBox="0 0 13 30" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><polyline id="fotoshmoto-left-arrow" stroke="#000000" transform="translate(6.336426, 14.830078) scale(1, -1) translate(-6.336426, -14.830078) " points="11.59375 0.33984375 1.07910156 14.6601563 11.59375 29.3203125"></polyline></g></svg>',
            '<svg width="13px" height="30px" viewBox="0 0 13 30" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><polyline id="fotoshmoto-right-arrow" stroke="#000000" transform="translate(6.336426, 14.830078) scale(-1, -1) translate(-6.336426, -14.830078) " points="11.59375 0.33984375 1.07910156 14.6601563 11.59375 29.3203125"></polyline></g></svg>'
        ],
        //autoWidth:true,
        dots: false,
        responsive:{
            0:{
                items:2
            },
            600:{
                items:3
            },
            1000:{
                items:5
            }
        }
    });
</script>

<!--<script type="text/javascript">// <![CDATA[-->
<!--    var fotoshmotoScript = document.createElement('script');-->
<!--    fotoshmotoScript.src = '//www.frisbuy.ru/fb/widget?username=varvara_design&count=120&mode=tile&cols=5&cols_min=2&title=&button=&spacing=4&load_more=0';-->
<!--    document.body.appendChild(fotoshmotoScript);-->
<!--    // ]]></script>-->
